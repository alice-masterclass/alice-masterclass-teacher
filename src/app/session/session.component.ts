import { Component, AfterViewInit, ViewChild, Pipe, PipeTransform } from '@angular/core';
import { Clipboard } from '@angular/cdk/clipboard';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';

import { EventAPI, SessionAPI, ApiService } from '../shared/services/api.service';

import { environment } from '../../environments/environment';
import { AddSessionDialogComponent } from './add-session-dialog/add-session-dialog.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { AddEventDialogComponent } from './add-event-dialog/add-event-dialog.component';

@Pipe({name: 'sessionUrl'})
export class SessionUrlPipe implements PipeTransform {
  transform(password: string): string {
    return `${environment.masterclassHost}?password=${encodeURIComponent(password)}`;
  }
}

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements AfterViewInit {

  private readonly URL_COPIED_DURATION = 500;

  public readonly displayedEventColumns: string[] = ['name', 'created', 'delete'];
  public readonly displayedSessionColumns: string[] = ['event', 'name', 'password', 'maxStudents', 'url', 'created', 'delete'];

  public eventTableRows: MatTableDataSource<EventAPI> = new MatTableDataSource<EventAPI>();
  public sessionTableRows: MatTableDataSource<SessionAPI> = new MatTableDataSource<SessionAPI>();

  public host!: string;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private clipboard: Clipboard,
    private apiService: ApiService,
    private urlPipe: SessionUrlPipe) { }

  ngAfterViewInit(): void {
    this.host = environment.masterclassHost;

    this.reload();

    if (this.apiService.autoRefresh()) {
      setInterval(()=> { this.reload(); }, this.apiService.REFRESH_INTERVAL);
    }
  }

  reload(): void {
    this.apiService.getEvents().subscribe((res: EventAPI[]) => {
      this.eventTableRows.data = res;
    }, (error: HttpErrorResponse) => {
      console.trace();
    });

    this.apiService.getSessions().subscribe((res: SessionAPI[]) => {
      this.sessionTableRows.data = res;
    });
  }

  onClipboardButtonClicked(elm: SessionAPI): void {
    this.snackBar.open('URL copied to clipboard', undefined, {duration: this.URL_COPIED_DURATION});

    this.clipboard.copy(this.urlPipe.transform(elm.password));
  }

  onEventDeleteButtonClicked(elm: EventAPI): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent).afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.apiService.deleteEvent(elm.id).subscribe(() => {
          this.reload();
        });
      } else {
        this.reload();
      }
    });
  }

  onSessionDeleteButtonClicked(elm: SessionAPI): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent).afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.apiService.deleteSession(elm.id).subscribe(() => {
          this.reload();
        });
      } else {
        this.reload();
      }
    });
  }

  onAddEventButtonClicked(): void {
    const dialogRef = this.dialog.open(AddEventDialogComponent).afterClosed().subscribe(() => {
      this.reload();
    });
  }

  onAddSessionButtonClicked(): void {
    const dialogRef = this.dialog.open(AddSessionDialogComponent).afterClosed().subscribe(() => {
      this.reload();
    });
  }

}
