export const environment = {
  production: true,
  apiUrl: 'https://api-alice-masterclass2.app.cern.ch/api/v1/',
  masterclassHost: 'https://alice-masterclass2.app.cern.ch/',
  openIdConfigUrl: 'https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration',
  redirectUri: '',
  clientId: ''
};
